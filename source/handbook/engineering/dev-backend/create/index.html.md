---
layout: markdown_page
title: "Create Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Create Team
{: #create}

The Create team works on the backend part of GitLab for the [Create product
category]. Among other things, this means working on GitLab's functionality around
source code management, code review, wiki, and snippets.

[Create product category]: /handbook/product/categories/#create